#include "date/datetime.h"
#include "i3/i3.h"
#include "i3/i3mode.h"
#include "i3/workspace.h"

#include <QApplication>
#include <QDebug>
#include <QQmlApplicationEngine>
#include <QQuickView>
#include <QWidget>

int
main(int argc, char* argv[])
{
  qmlRegisterType<DateTime>("Buffalo.Qt.Modules.Date", 1, 0, "DateTime");
  qmlRegisterType<i3>("Buffalo.Qt.Modules.I3", 1, 0, "WM");
  qmlRegisterType<i3Mode>("Buffalo.Qt.Modules.I3", 1, 0, "Mode");
  qmlRegisterType<Workspace>("Buffalo.Qt.Modules.I3", 1, 0, "Workspace");

  QApplication app(argc, argv);

  auto view = new QQuickView();
  auto widget = QWidget::createWindowContainer(view);

  widget->setWindowFlags(Qt::CustomizeWindowHint | Qt::FramelessWindowHint |
                         Qt::WindowStaysOnTopHint);

  widget->setAttribute(Qt::WA_X11NetWmWindowTypeDock);
  widget->setAttribute(Qt::WA_TranslucentBackground);
  widget->setAttribute(Qt::WA_AlwaysShowToolTips);
  widget->setAttribute(Qt::WA_X11DoNotAcceptFocus);

  widget->setFixedHeight(25);

  view->engine()->addImportPath(":/sh.hantz.buffalo");

  view->setResizeMode(QQuickView::SizeRootObjectToView);
  view->setColor(Qt::transparent);

  view->setSource(QUrl(u"qrc:/sh.hantz.buffalo/main/main.qml"_qs));

  widget->show();

  // qi3pc i3;
  // i3.connect();
  // qDebug() << "Version: " << i3.version()->first;
  // qDebug() << "get_workspaces: " << i3.workspaces();
  // qDebug() << "get_tree: " << i3.tree();

  // i3.subscribe({ "mode", "window", "workspace"});
  // qDebug() << "get_outputs: " << i3.outputs();
  // qDebug() << "get_marks: " << i3.marks();
  // qDebug() << "get_binding_modes: " << i3.binding_modes();

  return QApplication::exec();
}
