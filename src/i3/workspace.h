#ifndef WORKSPACE_H
#define WORKSPACE_H

#include <QColor>
#include <QDebug>
#include <QJsonObject>
#include <QObject>

class Workspace : public QObject
{
  Q_OBJECT
  Q_PROPERTY(int number READ number NOTIFY numberChanged)
  Q_PROPERTY(QString name READ name NOTIFY nameChanged)
  Q_PROPERTY(bool focused READ focused NOTIFY focusUpdated)
  Q_PROPERTY(bool urgent READ urgent NOTIFY urgencyUpdated)
  Q_PROPERTY(bool visible READ visible NOTIFY visibilityUpdated)
  Q_PROPERTY(QColor color READ color NOTIFY colorChanged)
public:
  explicit Workspace(const QJsonObject& jobj, QObject* parent = nullptr);
  explicit Workspace(QObject* parent = nullptr);
  bool operator<(const Workspace& w) const;
  bool operator==(const Workspace& w) const;
  bool operator==(const QJsonObject& jobj) const;
  void update(const Workspace& w);
  void update(const QJsonObject& jobj);
  void announceChanges();

  bool focused() const;
  void focused(bool focus);

  QString name() const;
  void name(const QString& name);

  int number() const;
  bool urgent() const;
  bool visible() const;
  QColor color() const;

private:
  void updateColor();

  QString m_name = "";
  QColor m_color = "white";
  int m_index = 0;
  bool m_focused = false;
  bool m_urgent = false;
  bool m_visible = false;

signals:
  void numberChanged();
  void nameChanged();
  void focusUpdated();
  void urgencyUpdated();
  void visibilityUpdated();
  void colorChanged();
};

#endif // WORKSPACE_H
