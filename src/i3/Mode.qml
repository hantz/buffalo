import QtQuick 2.0

import Buffalo.Qt.Modules.I3 1.0 as BuffaloQtI3

Rectangle {
    property BuffaloQtI3.Mode backend
    color: "darksalmon"
    height: 25
    width: text.width + 10
    visible: backend.visible

    Text {
        id: text
        color: "white"
        text: parent.backend.name
        anchors.centerIn: parent
    }
}
