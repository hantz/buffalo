import QtQuick 2.0
import QtQuick.Layouts 1.12
import Buffalo.Qt.Modules.I3 1.0 as BuffaloQtI3

Rectangle {
    property BuffaloQtI3.WM wm
    property alias windowName: windName

    radius: height / 3
    Layout.preferredWidth: windowName.text.length * windowName.font.pointSize
    Layout.maximumWidth: 31 * parent.width / 72
    Layout.preferredHeight: parent.height
    color: "transparent"

    Text {
        id: windName
        color: "white"
        text: wm.focusedWindowName
        elide: Text.ElideRight
        anchors.centerIn: parent
        width: parent.width - 10
    }
}
