#ifndef I3_H
#define I3_H

#include <QDebug>
#include <QJsonArray>
#include <QObject>
#include <QQmlListProperty>
#include <QThread>

#include <optional>

#include <qi3pc/qi3pc.h>

#include "i3mode.h"
#include "workspace.h"

class i3 : public QObject
{
  Q_OBJECT
  Q_PROPERTY(
    QString focusedWindowName READ focusedWindow NOTIFY focusedWindowChanged)
  Q_PROPERTY(QQmlListProperty<Workspace> workspaces READ workspaces NOTIFY
               workspacesChanged)
  Q_PROPERTY(i3Mode* mode READ mode NOTIFY modeChanged)

public:
  explicit i3(QObject* parent = nullptr);

public slots:
  void switchWorkspace(Workspace* workspace);

private:
  QString focusedWindow() const;
  std::optional<QString> getFocusedWindowName(const QJsonObject& tree) const;

private slots:
  void singleTreeUpdate(const qi3pc::DataObject& data);
  void singleWorkspaceUpdate(const qi3pc::DataArray& data);

  void updateFocusedWindow(const QJsonObject& tree);
  void windowEvent(qi3pc::WindowChange, const QJsonObject& container);

  void updateWorkspaces(const qi3pc::DataArray& data);
  void workspaceEvent(qi3pc::WorkspaceChange,
                      const QJsonObject& old,
                      const QJsonObject& current);

  void shutdownEvent(qi3pc::ShutdownChange change);

  void modeEvent(QString change, bool pango);

  QQmlListProperty<Workspace> workspaces();
  static qsizetype countWorkspaces(QQmlListProperty<Workspace>* list);
  static Workspace* getWorkspace(QQmlListProperty<Workspace>* list, qsizetype index);

  i3Mode* mode();

signals:
  void focusedWindowChanged();
  void workspacesChanged();
  void modeChanged();

private:
  QString m_focusedWindow;
  qi3pc m_ipc;
  QStringList m_subscriptions;
  QList<Workspace*> m_workspaces;
  i3Mode* m_mode;
};

#endif // I3_H
