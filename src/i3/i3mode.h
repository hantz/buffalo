#ifndef I3MODE_H
#define I3MODE_H

#include <QObject>

class i3Mode : public QObject
{
  Q_OBJECT
  Q_PROPERTY(QString name READ name NOTIFY textChanged)
  Q_PROPERTY(bool visible READ visible NOTIFY visibilityChanged)
public:
  explicit i3Mode(const QString& text = "default", QObject* parent = nullptr);
  void update(const QString& text);

private slots:

  QString name();
  void name(const QString& name);

  bool visible();
  void visible(bool visible);

signals:
  void textChanged();
  void visibilityChanged();

private:
  QString m_name;
  bool m_visible;
};

#endif // I3MODE_H
