import QtQuick 2.0

import Buffalo.Qt.Modules.I3 1.0 as BuffaloQtI3

Rectangle {
    property BuffaloQtI3.Workspace backend
    color: "transparent"
    height: 25
    width: ws.width
    radius: height / 3

    onWidthChanged: {
        width = ws.width < 25 ? 25 : ws.width
    }

    Text {
        id: ws
        color: backend.color
        text: backend.name.replace(/^[0-9]+:/, "")
        anchors.centerIn: parent
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            i3wm.switchWorkspace(modelData)
        }
    }
}
