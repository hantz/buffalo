#include "workspace.h"

Workspace::Workspace(const QJsonObject& jobj, QObject* parent)
  : QObject(parent)
{
  if (auto o = jobj["focused"]; !o.isUndefined()) {
    m_focused = o.toBool();
    updateColor();
  }

  if (auto o = jobj["urgent"]; !o.isUndefined())
    m_urgent = o.toBool();

  if (auto o = jobj["visible"]; !o.isUndefined())
    m_visible = o.toBool();

  if (auto o = jobj["name"]; !o.isUndefined())
    m_name = o.toString();

  if (auto o = jobj["num"]; !o.isUndefined())
    m_index = o.toInt();

  QObject::connect(
    this, &Workspace::focusUpdated, this, &Workspace::updateColor);
}

Workspace::Workspace(QObject* parent)
  : Workspace(QJsonObject(), parent)
{}

bool
Workspace::operator<(const Workspace& w) const
{
  return m_index < w.m_index;
}

bool
Workspace::operator==(const Workspace& w) const
{
  return m_index == w.m_index;
}

bool
Workspace::operator==(const QJsonObject& jobj) const
{
  return m_index == jobj["num"].toInt();
}

void
Workspace::update(const QJsonObject& jobj)
{
  focused(jobj["focused"].toBool());
  m_urgent = jobj["urgent"].toBool();
  name(jobj["name"].toString());
}

int
Workspace::number() const
{
  return m_index;
}

QString
Workspace::name() const
{
  return m_name;
}

void
Workspace::name(const QString& name)
{
  m_name = name;
  emit nameChanged();
}

bool
Workspace::focused() const
{
  return m_focused;
}

void
Workspace::focused(bool focus)
{
  if (m_focused != focus) {
    m_focused = focus;
    emit focusUpdated();
  }
}

bool
Workspace::urgent() const
{
  return m_urgent;
}

bool
Workspace::visible() const
{
  return m_visible;
}

QColor
Workspace::color() const
{
  return m_color;
}

void
Workspace::updateColor()
{
  if (m_focused) {
    m_color = "blue";
  } else if (m_urgent) {
    m_color = "red";
  } else {
    m_color = "white";
  }

  emit colorChanged();
}
