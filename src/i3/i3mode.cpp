#include "i3mode.h"

i3Mode::i3Mode(const QString& text, QObject* parent)
  : QObject(parent)
{
  name(text);
}

void
i3Mode::update(const QString& text)
{
  name(text);
}

void
i3Mode::name(const QString& text)
{
  if (text == m_name) {
    return;
  }

  m_name = text;

  emit textChanged();

  static QString def = "default";
  visible(m_name != def);
}

QString
i3Mode::name()
{
  return m_name;
}

void
i3Mode::visible(bool visible)
{
  m_visible = visible;
  emit visibilityChanged();
}

bool
i3Mode::visible()
{
  return m_visible;
}
