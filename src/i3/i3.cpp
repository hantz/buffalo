#include "i3.h"

i3::i3(QObject* parent)
  : QObject(parent)
{
  QObject::connect(&m_ipc, &qi3pc::workspaceEvent, this, &i3::workspaceEvent);
  QObject::connect(&m_ipc, &qi3pc::windowEvent, this, &i3::windowEvent);
  QObject::connect(&m_ipc, &qi3pc::shutdownEvent, this, &i3::shutdownEvent);
  QObject::connect(&m_ipc, &qi3pc::modeEvent, this, &i3::modeEvent);

  QObject::connect(
    &m_ipc, &qi3pc::workspacesUpdated, this, &i3::updateWorkspaces);

  m_ipc.connect();
  m_subscriptions.append(
    { "workspace", "window", "shutdown", "subscribe", "mode" });
  m_ipc.subscribe(m_subscriptions);

  m_ipc.fetchTree();
  QObject::connect(&m_ipc, &qi3pc::treeUpdated, this, &i3::singleTreeUpdate);

  m_ipc.fetchWorkspaces();
  QObject::connect(
    &m_ipc, &qi3pc::workspacesUpdated, this, &i3::singleWorkspaceUpdate);

  m_mode = new i3Mode("default", this);
}

void
i3::switchWorkspace(Workspace* workspace)
{
  QByteArray payload;
  QString payloadStr = "workspace " + workspace->name();
  payload.append(payloadStr.toStdString().c_str());
  m_ipc.sendMessage(qi3pc::IpcType::Command, payload);
}

void
i3::singleTreeUpdate(const qi3pc::DataObject& data)
{
  updateFocusedWindow(data->first);
  QObject::disconnect(&m_ipc, &qi3pc::treeUpdated, this, &i3::singleTreeUpdate);
}

void
i3::singleWorkspaceUpdate(const qi3pc::DataArray& data)
{
  updateWorkspaces(data);
  QObject::disconnect(
    &m_ipc, &qi3pc::workspacesUpdated, this, &i3::singleWorkspaceUpdate);
}

void
i3::workspaceEvent(qi3pc::WorkspaceChange change,
                   const QJsonObject& current,
                   const QJsonObject& old)
{
  Q_UNUSED(old)

  switch (short count = 0; change) {
    case qi3pc::WorkspaceChange::Focus:
      for (auto ws : m_workspaces) {
        if (*ws == current) {
          ws->focused(true);
          ++count;
        }

        if (*ws == old) {
          ws->focused(false);
          ++count;
        }

        if (count == 2) {
          break;
        }
      }
      emit workspacesChanged();
      break;
    case qi3pc::WorkspaceChange::Init:
      m_workspaces.append(new Workspace(current));
      emit workspacesChanged();
      break;
    case qi3pc::WorkspaceChange::Empty:
      for (auto ws : m_workspaces) {
        if (*ws == current) {
          m_workspaces.removeAll(ws);
          break;
        }
      }
      emit workspacesChanged();
      break;
    case qi3pc::WorkspaceChange::Urgent:
      for (auto ws : m_workspaces) {
        if (*ws == current) {
          ws->update(current);
        }
      }
      break;
    case qi3pc::WorkspaceChange::Move:
    // I have no setup to test and implement this as of now
    default:
    case qi3pc::WorkspaceChange::Rename:
      bool found = false;
      for (auto ws : m_workspaces) {
        if (*ws == current) {
          found = true;
          ws->update(current);
        }
      }

      // if the number have changed too
      if (!found) {
        m_ipc.fetchWorkspaces();
      }
      break;
  }
}

void
i3::windowEvent(qi3pc::WindowChange change, const QJsonObject& container)
{
  switch (change) {
    case qi3pc::WindowChange::Focus:
      updateFocusedWindow(container);
      break;
    case qi3pc::WindowChange::Title:
      if (container["focused"].toBool()) {
        updateFocusedWindow(container);
      }
      break;
    default:
      break;
  }
}

void
i3::shutdownEvent(qi3pc::ShutdownChange change)
{
  switch (change) {
    case qi3pc::ShutdownChange::Restart:
      // FIXME: ugh why do I need that? what am I missing?
      // I guess restarting the wm takes some time and the ipc
      // socket doesn't register connections until it's done
      // I can try to solve this with a tick event
      QThread::msleep(20);

      while (!m_ipc.disconnect())
        ;
      while (!m_ipc.connect())
        ;

      m_ipc.subscribe(m_subscriptions);
      break;
    default:
      break;
  }
}

void
i3::modeEvent(QString change, bool pango)
{
  Q_UNUSED(pango)

  m_mode->update(change);
  emit modeChanged();
}

void
i3::updateWorkspaces(const qi3pc::DataArray& data)
{
  QJsonArray ws = data->first;
  if (ws.empty()) {
    ws = m_ipc.workspaces()->first;
  }

  for (auto old : m_workspaces) {
    bool found = false;
    for (int i = 0; i < ws.size(); ++i) {
      if (*old == ws[i].toObject()) {
        old->update(ws[i].toObject());
        found = true;
        ws.removeAt(i);
        break;
      }
    }

    if (!found) {
      m_workspaces.removeAll(old);
    }
  }

  for (auto current : ws) {
    m_workspaces.append(new Workspace(current.toObject()));
  }

  emit workspacesChanged();
}

void
i3::updateFocusedWindow(const QJsonObject& tree)
{
  m_focusedWindow = getFocusedWindowName(tree).value_or(QString());

  emit focusedWindowChanged();
}

std::optional<QString>
i3::getFocusedWindowName(const QJsonObject& tree) const
{
  if (tree["focused"].toBool()) {
    return tree["name"].toString();
  }

  for (auto node : tree["nodes"].toArray()) {
    if (auto result = getFocusedWindowName(node.toObject()); result) {
      return result.value();
    }
  }

  return {};
}

QString
i3::focusedWindow() const
{
  return m_focusedWindow;
}

QQmlListProperty<Workspace>
i3::workspaces()
{
  return QQmlListProperty<Workspace>(
    this, &m_workspaces, &i3::countWorkspaces, &i3::getWorkspace);
}

qsizetype
i3::countWorkspaces(QQmlListProperty<Workspace>* list)
{
  return reinterpret_cast<i3*>(list->object)->m_workspaces.length();
}

Workspace*
i3::getWorkspace(QQmlListProperty<Workspace>* list, qsizetype index)
{
  return reinterpret_cast<i3*>(list->object)->m_workspaces.at(index);
}

i3Mode*
i3::mode()
{
  return m_mode;
}
