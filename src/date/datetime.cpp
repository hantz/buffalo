#include "datetime.h"

using namespace std::chrono_literals;

DateTime::DateTime(QObject* parent, DateTime::UpdateRate rate,
                   const QString& format)
  : QObject(parent),
    m_dateTime(QDateTime::currentDateTime())
{
	connect(&m_timer, &QTimer::timeout, this, &DateTime::tick);
	connect(this, &DateTime::clockRateChanged, this, &DateTime::synchronizeClock);
	connect(this, &DateTime::dateTimeUpdated, this, &DateTime::updateDateTimeString);
	connect(this, &DateTime::formatChanged, this, &DateTime::updateDateTimeString);

	setFormat(format);
	setClockRate(rate);
}

QString
DateTime::dateTimeString() const
{
	return m_dtString;
}

void
DateTime::updateDateTimeString()
{
	if (m_format == "epoch") {
		m_dtString = QString::number(m_dateTime.toSecsSinceEpoch());
	} else {
		m_dtString = m_dateTime.toString(m_format);
	}

	emit dateTimeStringUpdated();
}

QDateTime
DateTime::dateTime() const
{
	return m_dateTime;
}

DateTime::UpdateRate
DateTime::clockRate() const
{
	return m_clockRate;
}

void
DateTime::tick()
{
	m_dateTime = QDateTime::currentDateTime();
	emit dateTimeUpdated();
}

QString
DateTime::format() const
{
	return m_format;
}

void DateTime::setFormat(const QString &fmt)
{
	m_format = fmt;
	emit formatChanged();
}

void
DateTime::setClockRate(UpdateRate rate)
{
	m_clockRate = rate;

	auto interval = 1ms;
	switch (m_clockRate) {
		case UpdateRate::Hour:
			interval = 1h;
			break;
		case UpdateRate::Minute:
			interval = 1min;
			break;
		case UpdateRate::Second:
			interval = 1s;
			break;
		case UpdateRate::Millisecond:
			interval = 1ms;
			break;
	}
	m_timer.setInterval(interval);

	emit clockRateChanged();
}

void
DateTime::synchronizeClock()
{
	if (m_clockRate >= UpdateRate::Second) {
		qint64 interval = QDateTime::currentDateTime().msecsTo(nextTick());
		do {
			interval = QDateTime::currentDateTime().msecsTo(nextTick());
		} while(interval <= 0);
		QTimer::singleShot(interval, Qt::PreciseTimer, this, &DateTime::startClock);
	} else {
		startClock();
	}
}

QDateTime
DateTime::nextTick() const
{
	auto dt = QDateTime::currentDateTime();
	auto date = dt.date();
	int hours = dt.time().hour();
	int minutes = dt.time().minute();
	int seconds = dt.time().second();
	switch(m_clockRate) {
		case Hour:
			return QDateTime(date, QTime(hours + 1, 0));
			break;
		case Minute:
			return QDateTime(date, QTime(hours, minutes + 1));
			break;
		case Second:
			return QDateTime(date, QTime(hours, minutes, seconds + 1));
			break;
		case Millisecond:
		default:
			return QDateTime::currentDateTime().addMSecs(1);
			break;
	}
}

void
DateTime::startClock()
{
	tick();
	m_timer.start();
}
