import QtQuick.Controls 2.15
import QtQuick 2.12
import QtQuick.Layouts 1.12

import Buffalo.Qt.Modules.Date 1.0 as BuffaloQtDate

Rectangle {
    id: dateBG
    property alias textColor: text.color
    property var dateFormats: ["ddd dd MMM yyyy | hh:mm:ss", "ddd MMM d hh:mm:ss yyyy", "ddd MMMM d yy", "epoch", "hh:mm:ss.zzz", "h:m:s ap"]
    property int dateFormatIdx: 0
    property var foreground: text
    property BuffaloQtDate.DateTime date: BuffaloQtDate.DateTime {
        format: dateFormats[dateFormatIdx]
    }

    Layout.preferredWidth: foreground.width + 20
    Layout.preferredHeight: parent.height
    Layout.alignment: Qt.AlignHCenter
    radius: height / 3
    color: "transparent"

    Text {
        id: text
        text: date.dateTimeString
        anchors.centerIn: parent
        color: "white"
        onTextChanged: {
            dateBG.width = width + 20
        }
    }

    MouseArea {
        anchors.fill: parent
        onClicked: {
            dateFormatIdx = (dateFormatIdx + 1) % dateFormats.length;
        }
    }
}
