#ifndef DATEH
#define DATEH

#include <QCalendar>
#include <QDateTime>
#include <QObject>
#include <QTimer>

// TODO: sync with system clock using a second (singleshot) timer to start the real timer
class DateTime : public QObject
{
	Q_OBJECT
	Q_PROPERTY(QDateTime dateTime READ dateTime NOTIFY dateTimeUpdated)
	Q_PROPERTY(QString dateTimeString READ dateTimeString NOTIFY dateTimeStringUpdated)
	Q_PROPERTY(QString format READ format WRITE setFormat NOTIFY formatChanged)
	Q_PROPERTY(UpdateRate clockRate READ clockRate WRITE setClockRate
	           NOTIFY clockRateChanged)

public:
	enum UpdateRate
	{
		Millisecond,
		Second,
		Minute,
		Hour
	};
	Q_ENUM(UpdateRate)

	explicit DateTime(QObject *parent = nullptr,
	                  DateTime::UpdateRate rate = DateTime::Second,
	                  const QString& format = "ddd MMM d hh:mm:ss yyyy");

	QDateTime dateTime() const;

	QString format() const;
	void setFormat(const QString& fmt);

	UpdateRate clockRate() const;
	void setClockRate(UpdateRate rate);

	QString dateTimeString() const;

private:
	QDateTime nextTick() const;

private slots:
	void startClock();
	void synchronizeClock();
	void tick();
	void updateDateTimeString();

private:
	QTimer m_timer;
	QDateTime m_dateTime;
	UpdateRate m_clockRate;
	QString m_format;
	QString m_dtString;

signals:
	void dateTimeUpdated();
	void dateTimeStringUpdated();
	void clockRateChanged();
	void formatChanged();
};

#endif // DATEH
