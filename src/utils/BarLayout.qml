import QtQuick 2.0
import QtQuick.Layouts 1.12

RowLayout {
    id: bar
    property RowLayout leftPane: RowLayout {
        parent: bar
        Layout.preferredWidth: bar.width / 3
        Layout.preferredHeight: bar.height
        Layout.alignment: Qt.AlignLeft
    }
    property RowLayout centerPane: RowLayout {
        parent: bar
        Layout.preferredWidth: bar.width / 3
        Layout.preferredHeight: bar.height
        Layout.alignment: Qt.AlignCenter
    }

    property RowLayout rightPane: RowLayout {
        parent: bar
        Layout.preferredWidth: bar.width / 3
        Layout.preferredHeight: bar.height
        Layout.alignment: Qt.AlignRight
    }
}
