qt_add_library(BuffaloUtils STATIC)

set_target_properties(BuffaloUtils PROPERTIES AUTOMOC ON)
target_link_libraries(BuffaloUtils PRIVATE Qt6::Quick)

list(APPEND BUFFALOUTILS_QML_FILES BarLayout.qml)

list(APPEND BUFFALOUTILS_CPP_FILES)

qt_add_qml_module(BuffaloUtils
  URI Buffalo.Quick.Utils
  VERSION 1.0
  RESOURCE_PREFIX /sh.hantz.buffalo
  QML_FILES
    ${BUFFALOUTILS_QML_FILES}
  SOURCES
    ${BUFFALOUTILS_CPP_FILES}
)
