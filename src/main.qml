import QtQuick 2.12
import QtQuick.Layouts 1.12

import Buffalo.Quick.Utils 1.0 as BuffaloQuickUtils
import Buffalo.Quick.Modules.Date 1.0 as BuffaloQuickDate
import Buffalo.Quick.Modules.I3 1.0 as BuffaloQuickI3

import Buffalo.Qt.Modules.I3 1.0 as BuffaloQtI3

BuffaloQuickUtils.BarLayout {
    id: bar
    anchors.fill: parent
    property BuffaloQtI3.WM i3wm: BuffaloQtI3.WM {
    }

    leftPane.children: [
        RowLayout {
            Layout.alignment: Qt.AlignLeft
            // anchors.fill: parent
            BuffaloQuickI3.Focus {
                wm: i3wm
            }
            Row {
                spacing: 3
                Repeater {
                    model: i3wm.workspaces

                     BuffaloQuickI3.Workspace {
                        backend: modelData
                     }
                }
            }

             BuffaloQuickI3.Mode {
                backend: i3wm.mode
             }
        }
    ]

    centerPane.children: [
        BuffaloQuickDate.Date {
        }
    ]
}

